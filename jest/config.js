
const path = require('path')

module.exports = {
  rootDir: '.',
  collectCoverage: true,
  collectCoverageFrom: [
    'packages/*/src/**/*.ts',
    'src/*/src/**/*.ts',
    '!**/*.d.ts',
    '!**/index.ts'
  ],
  setupFiles: [
    path.join(__dirname, 'setup.js'),
  ],
  testMatch: [
    '<rootDir>/packages/**/*.test.ts?(x)',
    '<rootDir>/src/**/*.test.ts?(x)',
  ],
  transform: {
    '^.+\\.ts$': 'ts-jest'
  },
  moduleDirectories: [
    'node_modules', '<rootDir>/packages'
  ],
  transformIgnorePatterns: [
    '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$',
  ],
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js',
    'jsx',
    'json',
  ],
  globals: {
    '__DEV__': true,
  },
  coverageThreshold: {
    global: {
      statements: 0,
      branches: 0,
      lines: 0,
      functions: 0,
    },
  },
}
