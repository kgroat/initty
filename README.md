# Initty
<!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->
[![All Contributors](https://img.shields.io/badge/all_contributors-1-blue.svg?style=flat-square)](#contributors)
<!-- ALL-CONTRIBUTORS-BADGE:END -->
A command-line utility for authoring starter projects

## Rationale
There are projects like
[`create-react-app`](https://github.com/facebook/create-react-app),
[`create-esm`](https://github.com/standard-things/create-esm), and
[`create-deck`](https://github.com/jxnblk/mdx-deck/tree/master/packages/create-deck)
that exist in order to allow users to create a project from some starting template.

It should be easy to create and maintain starter projects.  `initty` aims to make this a reality.

## Contributors

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tr>
    <td align="center"><a href="https://gitlab.com/kgroat"><img src="https://secure.gravatar.com/avatar/5deb3b51238c3948e41f366ce9bb53fa?s=80&d=identicon" width="100px;" alt="Kevin Groat"/><br /><sub><b>Kevin Groat</b></sub></a><br /><a href="https://gitlab.com/kgroat/initty/commits/master" title="Code">💻</a> <a href="https://gitlab.com/kgroat/initty/commits/master" title="Documentation">📖</a></td>
  </tr>
</table>

<!-- markdownlint-enable -->
<!-- prettier-ignore-end -->
<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification. Contributions of any kind welcome!