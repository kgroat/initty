#!/usr/bin/env node

const path = require('path')

process.env.TS_NODE_PROJECT = path.join(__dirname, 'tsconfig.json')
require('ts-node/register')

const { fromDir } = require('@initty/from-dir')

fromDir({
  source: __dirname,
  destination: process.argv[2],
}).catch(e => {
  console.error(e)
  process.exit(1)
})
