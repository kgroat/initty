
import { buildConfig, GivenParams } from '@initty/common'
import { version } from './params'

interface Params extends GivenParams {
  version: string
  camelCaseName: () => string
  name: () => string
  description: string
}

export default buildConfig<Params>({
  parser: 'mustache',
  initialValues: {
    version,
    camelCaseName (this: Params) {
      return this.projectName.replace(
        /-(\w)/g,
        (_, letter: string) => letter.toUpperCase(),
      )
    },
    name (this: Params) {
      return `@initty/${this.projectName}`
    },
  },
  copyRoot: 'template',
  prompt: [
    {
      name: 'description',
      type: 'input',
      message: 'What is the description?'
    },
  ]
})
