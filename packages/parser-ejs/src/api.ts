
import { render } from 'ejs'
import { ParserFunc } from '@initty/common'

const extRgx = /^(.*)\.(:?ejs)/
const ejsParser: ParserFunc<any> = async ({ path, data, mode }, params) => {
  if (extRgx.test(path)) {
    path = extRgx.exec(path)![1]
  }
  return {
    path: render(path, params),
    data: render(data, params),
    mode,
  }
}

export default ejsParser
