#!/usr/bin/env node

const path = require('path')
const project = path.join(__dirname, 'tsconfig.json')

process.env.TS_NODE_PROJECT = project
require('ts-node/register')

require('./src')
