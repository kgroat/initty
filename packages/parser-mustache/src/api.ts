
import * as mustacheType from 'mustache'
import { ParserFunc } from '@initty/common'

const mustache = require('mustache') as typeof mustacheType
mustache.escape = (str) => str

const extRgx = /^(.*)\.(:?mustache|mst|mu|stache)/
const mustacheParser: ParserFunc<any> = ({ path, data, mode }, params) => {
  if (extRgx.test(path)) {
    path = extRgx.exec(path)![1]
  }
  return {
    path: mustache.render(path, params),
    data: mustache.render(data, params),
    mode,
  }
}

export default mustacheParser
