#!/usr/bin/env node
const importLocal = require('import-local')
import api from './api'

if (importLocal(__filename)) {
  console.log('Using local version of this package')
} else {
  api().catch(e => {
    console.error(e)
    process.exit(e.code || 1)
  })
}
