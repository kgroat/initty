
import { join, sep } from 'path'

export function joinRoot (...args: string[]) {
  const idxToStartFrom = args.reduceRight(
    (currIdx, curr, idx) => {
      if (currIdx >= 0) {
        return currIdx
      } else if (curr[0] === sep) {
        return idx
      } else {
        return -1
      }
    },
    -1,
  )

  return join(...args.slice(idxToStartFrom))
}
