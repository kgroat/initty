
import { prompt } from 'enquirer'
import * as ansi from 'ansi-colors'

export type Parser = 'none' | 'mustache' | 'ejs'

export interface FileData {
  path: string
  data: string
  mode: number
}

export interface ParserFunc<T> {
  (data: FileData, params: T): FileData | Promise<FileData>
}

export interface ParserMap<T> {
  [file: string]: ParserFunc<T>
}

export type ArgType<T> = T extends (arg: infer P) => any ? P : never

export interface GivenParams {
  projectName: string
}

export interface InittyConfig<T extends GivenParams = any> {
  installCommand?: string | null
  parser?: Parser | ParserFunc<T>
  prompt?: ArgType<typeof prompt> | null
  initialValues?: Partial<T> | (() => Partial<T>) | (() => Promise<Partial<T>>)
  includeFiles?: string | string[]
  ignoreFiles?: string | string[]
  noParse?: string | string[]
  copyRoot?: string
  preparser?: ParserFunc<T> | null
  postparser?: ParserFunc<T> | null
  preparserMap?: ParserMap<T> | null
  postparserMap?: ParserMap<T> | null
}

export type FullInittyConfig<T extends GivenParams> = Required<InittyConfig<T>> & {
  includeFiles: string[]
  ignoreFiles: string[]
  noParse: string[]
}

type KeyOfType<O, K> = { [key in keyof O]: O[key] extends K ? key : never }[keyof O]
type ConfigPropOfType<T> = KeyOfType<FullInittyConfig<any>, T>

export type ConfigProp = keyof FullInittyConfig<any>
export type FileGlobProp = ConfigPropOfType<string[]>
export type ParserProp = ConfigPropOfType<Parser | ParserFunc<any>>
export type ParserFuncProp = ConfigPropOfType<ParserFunc<any> | null>
export type ParserMapProp = ConfigPropOfType<ParserMap<any> | null>

type AnyKey = keyof any
type AnyKeys = AnyKey | AnyKey[]
export interface ValidationResult<Prop extends AnyKeys = AnyKeys> {
  message: string
  property: Prop
}

export class ValidationError extends Error {
  constructor (
    public readonly errors: ValidationResult[],
    public readonly objectName?: string,
  ) {
    super(`${errors.length} errors found${objectName ? ` with ${objectName}` : ''}`)
  }

  logErrors () {
    console.error(ansi.bold.red(`${this.message}`))
    this.errors.forEach(({ property, message }) => {
      let propertyName: string
      if (Array.isArray(property)) {
        propertyName = property.join('.')
      } else {
        propertyName = property.toString()
      }

      console.error(` * ${ansi.bold.red(propertyName)} -- ${ansi.bold(message)}`)
    })
  }
}
