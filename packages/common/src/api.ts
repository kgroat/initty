
const INITTY_DEBUG = process.env.INITTY_DEBUG
export const DEBUG = !!INITTY_DEBUG && INITTY_DEBUG !== 'false'

export * from './applyValidationMap'
export * from './config/buildConfig'
export * from './config/readConfig'
export * from './config/validateConfig'
export * from './types'
export * from './joinRoot'
