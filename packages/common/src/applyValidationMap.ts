import { ValidationResult } from './types'

type AnyKey = keyof any
type AnyKeys = AnyKey | AnyKey[]
export type PropertyValidator<
  T,
  K extends keyof T,
  PropType extends AnyKeys = K
> = (obj: T, prop: K) => ValidationResult<PropType>[]

export type ValidationMap<T, PropType extends AnyKeys = keyof T> = {
  [prop in keyof T]: PropertyValidator<T, prop, PropType>
}

export const noValidation = (): ValidationResult<any>[] => []

export function applyValidationMap<T, PropType extends AnyKeys> (obj: T, validation: ValidationMap<T, PropType>): ValidationResult<PropType>[] {
  const errors: ValidationResult<PropType>[] = []

  const objProps = Object.keys(obj) as (keyof T)[]
  const validationProps = Object.keys(validation) as (keyof T)[]

  objProps.forEach(property => {
    if (!validationProps.includes(property)) {
      errors.push({
        property: property as string as PropType,
        message: `unrecognized property; valid properties are [${validationProps.join(', ')}]`,
      })
    }
  })

  validationProps.forEach(property => {
    errors.push(...validation[property](obj, property))
  })

  return errors
}
