
import { existsSync, statSync } from 'fs'
import { buildConfig } from './buildConfig'
import { InittyConfig, FullInittyConfig } from '../types'
import { joinRoot } from '../joinRoot'

export const configFiles = [
  'initty/config.ts',
  'initty/config.js',
  'initty/config.json',
  'initty.ts',
  'initty.js',
  'initty.json',
]

type InittyConfigFileContent = InittyConfig<any> & { default: InittyConfig<any> }

export function readConfig (source: string): FullInittyConfig<any> {
  if (!existsSync(source)) {
    throw new Error(`Source ${source} does not exist`)
  }
  let sourceStats = statSync(source)
  if (sourceStats.isDirectory()) {
    const sourceFile = configFiles.map(f => joinRoot(source, f)).find(existsSync)
    if (sourceFile === undefined) {
      throw new Error(`Could not find a config file.  Valid config filenames are ${configFiles.join(', ')}`)
    }
    source = sourceFile
    sourceStats = statSync(source)
  }

  if (!sourceStats.isFile()) {
    throw new Error(`${source} is not a file`)
  }

  let config: InittyConfig<any>

  try {
    const configContent = require(source) as InittyConfigFileContent
    if (configContent.default) {
      config = configContent.default
    } else {
      config = configContent
    }
  } catch (e) {
    throw new Error(`Error while reading config file ${source} --\n${e.message | e}`)
  }

  const fullConfig = buildConfig(config)

  return fullConfig
}
