import {
  FullInittyConfig,
  Parser,
  FileGlobProp,
  ConfigProp,
  ParserMapProp,
  ParserFunc,
  ParserFuncProp,
  ParserProp,
  ValidationResult,
} from '../types'
import { ValidationMap, applyValidationMap, noValidation } from '../applyValidationMap'

type ConfigValidationProperty =
  | ConfigProp
  | [FileGlobProp, number]
  | [ParserMapProp, string]

type ConfigError = ValidationResult<ConfigValidationProperty>

type ConfigValidation = ValidationMap<FullInittyConfig<any>, ConfigValidationProperty>

const parsers: Parser[] = [
  'none',
  'mustache',
  'ejs',
]

function validateInstallCommand (config: FullInittyConfig<any>): ConfigError[] {
  const errors: ConfigError[] = []
  const command = config.installCommand

  if (command !== null && typeof (command as any) !== 'string') {
    errors.push({
      property: 'installCommand',
      message: `should be a string, instead found ${command}`,
    })
  }

  return errors
}

function validateParser (config: FullInittyConfig<any>, property: ParserProp): ConfigError[] {
  const errors: ConfigError[] = []
  const parser = config[property]

  if (typeof parser === 'string') {
    if (!parsers.includes(parser)) {
      errors.push({
        property,
        message: `should be one of [${parsers.join(', ')}] -- instead was ${parser}`,
      })
    }
  } else if (typeof (parser as any) !== 'function') {
    errors.push({
      property,
      message: `should be either one of [${parsers.join(', ')}] or a parser function -- instead was ${parser}`,
    })
  }

  return errors
}

function validateGlobs (config: FullInittyConfig<any>, property: FileGlobProp): ConfigError[] {
  const errors: ConfigError[] = []
  const globs = config[property]

  if (!Array.isArray(globs)) {
    errors.push({
      property,
      message: `should be an array of strings, instead was ${globs}`,
    })
  } else {
    if (property === 'includeFiles' && globs.length < 1) {
      errors.push({
        property,
        message: `should have at least one file or globbing pattern specified, but had length 0`,
      })
    }
    globs.forEach((glob, i) => {
      if (typeof (glob as any) !== 'string') {
        errors.push({
          property: [property, i],
          message: `should be a string, instead was ${glob}`,
        })
      }
    })
  }

  return errors
}

function validateParserFunc (parser: ParserFunc<any> | null | undefined, property: ParserFuncProp | [ParserMapProp, string]): ConfigError[] {
  const errors: ConfigError[] = []

  if (parser && typeof (parser as any) !== 'function') {
    errors.push({
      property,
      message: `should be a function, instead found ${parser}`,
    })
  }

  return errors
}

function validateParserProp (config: FullInittyConfig<any>, property: ParserFuncProp): ConfigError[] {
  return validateParserFunc(config[property], property)
}

function validateParserMap (config: FullInittyConfig<any>, property: ParserMapProp): ConfigError[] {
  const errors: ConfigError[] = []

  const map = config[property]
  if (map === null) {
    return []
  }
  return Object.keys(map).reduce((errs, glob) => [
    ...errs,
    ...validateParserFunc(map[glob], [property, glob]),
  ], errors)
}

function validateIsString (config: FullInittyConfig<any>, property: ConfigProp): ConfigError[] {
  const errors: ConfigError[] = []
  const value = config[property]

  if (typeof value !== 'string') {
    errors.push({
      property,
      message: `should be a string, instead found ${value}`,
    })
  }

  return errors
}

const configValidation: ConfigValidation = {
  installCommand: validateInstallCommand,
  parser: validateParser,
  prompt: noValidation,
  initialValues: noValidation, // TODO: initial values validation
  copyRoot: validateIsString,
  includeFiles: validateGlobs,
  ignoreFiles: validateGlobs,
  noParse: validateGlobs,
  preparser: validateParserProp,
  postparser: validateParserProp,
  preparserMap: validateParserMap,
  postparserMap: validateParserMap,
}

export const validateConfig = (config: FullInittyConfig<any>) => (
  applyValidationMap(config, configValidation)
)
