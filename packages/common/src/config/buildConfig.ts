import { InittyConfig, FullInittyConfig, GivenParams } from '../types'

const alreadyBuilt = Symbol('already built')

function ensureArray<T> (input: T | T[]): T[] {
  return Array.isArray(input) ? input : [input]
}

type AlreadyBuiltConfig<T extends GivenParams> = FullInittyConfig<T> & {
  [alreadyBuilt]: boolean
}

export function buildConfig<T extends GivenParams> (config: InittyConfig<T>): FullInittyConfig<T> {
  if ((config as AlreadyBuiltConfig<T>)[alreadyBuilt]) {
    return config as AlreadyBuiltConfig<T>
  }

  const {
    parser = 'none',
    includeFiles = ['**/*'],
    ignoreFiles = [],
    noParse = [],
  } = config

  const output: AlreadyBuiltConfig<T> = {
    [alreadyBuilt]: true,
    installCommand: 'npm i',
    parser,
    prompt: null,
    postparser: null,
    preparser: null,
    postparserMap: null,
    preparserMap: null,
    copyRoot: '.',
    initialValues: {},
    ...config,
    includeFiles: ensureArray(includeFiles),
    ignoreFiles: ensureArray(ignoreFiles),
    noParse: ensureArray(noParse),
  }

  return output
}
