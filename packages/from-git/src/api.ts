
import { fromDir } from '@initty/from-dir'
import { cloneRepository } from './helpers/cloneRepository'

export interface FromGitOpts {
  repository: string
  destination: string
}

export async function fromGit (opts: FromGitOpts): Promise<void> {
  const { repository, destination } = opts

  console.log(`Cloning ${repository}`)
  const source = await cloneRepository(repository)
  await fromDir({
    source,
    destination,
  })
}
