
export class CloneExitError extends Error {
  constructor (
    public repository: string,
    public exitCode: number,
  ) {
    super(`Failed to clone ${repository} -- git exited with code ${exitCode}`)
  }
}

export class CloneSignalError extends Error {
  constructor (
    public repository: string,
    public signal: string,
  ) {
    super(`Failed to clone ${repository} -- git was aborted with signal ${signal}`)
  }
}

export class CloneOtherError extends Error {
  constructor (
    public repository: string,
    public cause: Error,
  ) {
    super(`Failed to clone ${repository} -- failed with error`)
  }
}
