
import { spawn, ChildProcess } from 'child_process'
import { createHash } from 'crypto'
import { stat } from 'fs'
import { tmpdir } from 'os'
import { join } from 'path'
import { promisify } from 'util'
import { CloneSignalError, CloneExitError, CloneOtherError } from './CloneErrors'

const statAsync = promisify(stat)
const existsAsync = (file: string) => (
  statAsync(file).then(
    () => true,
    () => false,
  )
)

const hash = createHash('sha256')

function waitOnChild (child: ChildProcess, repository: string) {
  return new Promise((resolve, reject) => {
    let done = false
    child.on('error', (err) => {
      if (!done) {
        reject(new CloneOtherError(repository, err))
      }
    })
    child.on('exit', (code, signal) => {
      if (code !== null && code > 0) {
        reject(new CloneExitError(repository, code))
      } else if (signal !== null) {
        reject(new CloneSignalError(repository, signal))
      } else {
        resolve()
      }
    })
  })
}

function gitPull (repository: string, destination: string) {
  return waitOnChild(
    spawn('git', ['pull', '-q', '--progress'], { cwd: destination, stdio: 'inherit' }),
    repository,
  )
}

function gitClone (repository: string, destination: string) {
  return waitOnChild(
    spawn('git', ['clone', repository, destination, '-q', '--progress'], { stdio: 'inherit' }),
    repository,
  )
}

export async function cloneRepository (repository: string): Promise<string> {
  hash.update(repository)
  const destination = join(tmpdir(), hash.digest('hex'))
  const destinationExists = await existsAsync(destination)

  if (destinationExists) {
    await gitPull(repository, destination)
  } else {
    await gitClone(repository, destination)
  }

  return destination
}
