#!/usr/bin/env node

import { fromGit } from './api'

if (process.argv.length < 4) {
  console.error('Please supply a repository and a destination')
  process.exit(1)
}

fromGit({ repository: process.argv[2], destination: process.argv[3] }).catch(e => {
  console.error(e)
  process.exit(e.code || 1)
})
