#!/usr/bin/env node

import { fromDir } from './api'
import { ValidationError } from '@initty/common'

if (process.argv.length < 4) {
  console.error('Please supply a source and a destination')
  process.exit(1)
}

fromDir({ source: process.argv[2], destination: process.argv[3] }).catch(e => {
  if (e instanceof ValidationError) {
    e.logErrors()
  } else {
    console.error(e)
  }
  process.exit(e.code || 1)
})
