
import { mkdir, existsSync } from 'fs'
import { promisify } from 'util'
import * as enquirer from 'enquirer'
import * as minimatch from 'minimatch'
import { joinRoot, readConfig, validateConfig, ValidationError } from '@initty/common'

import { buildParser } from './helpers/buildParser'
import { getAllFilesFromConfig } from './helpers/getAllFilesFromConfig'
import { notAny } from './helpers/notAny'
import { getFileData } from './helpers/getFileData'
import { writeFiles } from './helpers/writeFiles'
import { loadTsnode } from './helpers/loadTsnode'
import { getParams } from './helpers/getParams'

const mkdirAsync = promisify(mkdir)

export interface FromDirOpts {
  source: string
  destination?: string
  defaultDest?: string
}

async function ensureDestination (destination?: string, defaultDest = 'my-project'): Promise<string> {
  if (destination) {
    return destination
  } else {
    return (await enquirer.prompt<{ destination: string }>({
      name: 'destination',
      type: 'input',
      message: 'Project name',
      initial: defaultDest,
    })).destination
  }
}

export async function fromDir (opts: FromDirOpts) {
  const { source, destination, defaultDest } = opts
  const trueDest = await ensureDestination(destination, defaultDest)
  const fullSource = joinRoot(process.cwd(), source)
  const fullDest = joinRoot(process.cwd(), trueDest)
  loadTsnode(fullSource)

  const config = readConfig(fullSource)
  const validationResults = validateConfig(config)
  if (validationResults.length > 0) {
    throw new ValidationError(validationResults, 'initty config')
  }

  const copyRoot = joinRoot(fullSource, config.copyRoot)
  const noParse = config.noParse.length > 0 ? notAny(config.noParse.map(g => minimatch.filter(g))) : () => false
  const params = await getParams(config, trueDest)

  if (existsSync(fullDest)) {
    throw new Error(`${fullDest} already exists -- aborting`)
  }

  console.log(`Initializing ${fullDest} ...`)
  await mkdirAsync(fullDest, { recursive: true })

  const parser = buildParser(config)
  const allFiles = await getAllFilesFromConfig(config, copyRoot)
  const fileData = await getFileData(copyRoot, allFiles, params, parser, noParse)
  await writeFiles(fullDest, fileData)
}
