
import * as tsnode from 'ts-node'
import { join } from 'path'

export function loadTsnode (sourceDir: string) {
  console.log('loadTsnode')
  try {
    const { register, REGISTER_INSTANCE }: typeof tsnode = require('ts-node')
    const existingRegister = process[REGISTER_INSTANCE as keyof typeof process] as tsnode.Register
    if (!existingRegister) {
      console.log('registering ts-node...')
      register({
        project: join(sourceDir, 'tsconfig.json'),
      })
      console.log('done registering ts-node')
    } else {
      console.log('ts-node already registered')
    }
  } catch {
    console.log('ts-node not found')
    // do nothing
  }
}
