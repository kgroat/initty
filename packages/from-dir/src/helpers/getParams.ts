
import * as enquirer from 'enquirer'
import { FullInittyConfig, GivenParams } from '@initty/common'

export async function getParams<T extends GivenParams> (config: FullInittyConfig<T>, projectName: string): Promise<T> {
  return {
    ...config.initialValues,
    ...config.prompt !== null ? await enquirer.prompt<T>(config.prompt) : {} as T,
    projectName,
  }
}
