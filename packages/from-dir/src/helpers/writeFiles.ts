
import { dirname } from 'path'
import { writeFile, mkdir, stat } from 'fs'
import { promisify } from 'util'
import { FileOrDirOrBufferData } from '../types'

import { joinRoot } from '@initty/common'

const writeFileAsync = promisify(writeFile)
const mkdirAsync = promisify(mkdir)
const statAsync = promisify(stat)
const existsAsync = (file: string) => (
  statAsync(file).then(
    () => true,
    () => false,
  )
)

const waitTime = 100
const maxWaits = 100

const sleep = (time: number) => new Promise(resolve => setTimeout(resolve, time))

export async function writeFiles (dest: string, files: FileOrDirOrBufferData[]) {
  await Promise.all(files.map(async (file) => {
    const { path, mode } = file
    let fullPath = joinRoot(dest, path)
    while (fullPath.endsWith('/') || fullPath.endsWith('\\')) {
      fullPath = fullPath.substring(0, fullPath.length - 1)
    }
    const parent = dirname(fullPath)
    let currentCycle = 0
    while (!await existsAsync(parent)) {
      if (currentCycle === 0) {
        // console.log(`Waiting for ${parent} to exist before creating ${path}`)
      }
      if (++currentCycle > maxWaits) {
        throw new Error(`Timed out waiting for ${parent} to exist`)
      }
      await sleep(waitTime)
    }

    if (file.dir) {
      console.log(`Creating dir ${path}`)
      await mkdirAsync(fullPath, { mode })
    } else {
      console.log(`Writing ${path}`)
      await writeFileAsync(fullPath, file.data, { mode })
    }
  }))
}
