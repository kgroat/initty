import { FullInittyConfig, Parser, ParserFunc, ParserMap } from '@initty/common'

import * as minimatch from 'minimatch'

const identity = <T>(d: T) => d
function getParserFunc (parser: Parser | ParserFunc<any>): ParserFunc<any> {
  if (parser === 'none') {
    return identity
  } else if (parser === 'ejs') {
    console.log('Using @initty/parser-ejs')
    return require('@initty/parser-ejs').default
  } else if (parser === 'mustache') {
    console.log('Using @initty/parser-mustache')
    return require('@initty/parser-mustache').default
  } else if (typeof (parser as any) === 'string') {
    return require(parser as any).default
  } else {
    return parser
  }
}

function buildParsersFromMap (parserMap: ParserMap<any>): ParserFunc<any>[] {
  const parserMatchers = Object.keys(parserMap).map(key => {
    return {
      matcher: (path: string) => minimatch(path, key),
      parser: parserMap[key],
    }
  })
  return parserMatchers.map(({ matcher, parser }) => {
    return (data, params) => {
      if (matcher(data.path)) {
        return parser(data, params)
      }

      return data
    }
  })
}

export function buildParser (config: FullInittyConfig<any>): ParserFunc<any> {
  const {
    parser,
    preparser,
    postparser,
    preparserMap,
    postparserMap,
  } = config

  const parserList: ParserFunc<any>[] = []

  if (preparser !== null) {
    parserList.push(preparser)
  }
  if (preparserMap !== null) {
    parserList.push(...buildParsersFromMap(preparserMap))
  }
  parserList.push(getParserFunc(parser))
  if (postparser !== null) {
    parserList.push(postparser)
  }
  if (postparserMap !== null) {
    parserList.push(...buildParsersFromMap(postparserMap))
  }

  return (fileData, params) => (
    parserList.reduce(
      (prev, curr) => prev.then(data => curr(data, params)),
      Promise.resolve(fileData),
    )
  )
}
