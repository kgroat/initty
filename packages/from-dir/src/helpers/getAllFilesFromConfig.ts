
import { promisify } from 'util'
import * as glob from 'glob'
import * as minimatch from 'minimatch'

const globAsync = promisify(glob)

import { FullInittyConfig, configFiles } from '@initty/common'
import { unique } from './unique'
import { notAny } from './notAny'

async function getAllGlobbedFiles (globs: string[], cwd: string) {
  const globResults = await Promise.all(globs.map(g => globAsync(g, { cwd })))
  return unique(globResults.reduce((acc, curr) => [...acc, ...curr]))
}

export async function getAllFilesFromConfig (config: FullInittyConfig<any>, source: string): Promise<string[]> {
  let { includeFiles, ignoreFiles } = config
  ignoreFiles = [
    ...ignoreFiles,
    ...includeFiles.filter(g => g.startsWith('!')).map(g => g.substring(1)),
    ...configFiles,
    'initty/**',
  ]
  includeFiles = includeFiles.filter(g => !g.startsWith('!'))

  const ignoreFileGlobs = ignoreFiles.map(g => minimatch.filter(g))
  const allFiles = await getAllGlobbedFiles(includeFiles, source)

  return allFiles.filter(notAny(ignoreFileGlobs))
}
