
export function unique<T> (input: T[]): T[] {
  return input.filter((item, idx) => !input.slice(0, idx).includes(item))
}
