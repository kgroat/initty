
export function notAny<T> (matchers: ((t: T, i: number, arr: readonly T[]) => boolean)[]): (t: T, i?: number, arr?: readonly T[]) => boolean {
  return (t: T, i?: number, arr?: readonly T[]) => !matchers.some(matcher => matcher(t, i as number, arr as T[]))
}
