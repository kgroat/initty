
import { readFile, stat } from 'fs'
import { promisify } from 'util'

const readFileAsync = promisify(readFile)
const statAsync = promisify(stat)

import { ParserFunc, joinRoot } from '@initty/common'
import { FileOrDirOrBufferData } from '../types'

export function getFileData (source: string, files: string[], params: any, parser: ParserFunc<any>, noParse: (path: string) => boolean) {
  return Promise.all(files.map(async (path): Promise<FileOrDirOrBufferData> => {
    const fullPath = joinRoot(source, path)
    const stats = await statAsync(fullPath)
    const { mode } = stats
    if (stats.isDirectory()) {
      return {
        path,
        mode,
        dir: true,
      }
    }

    const buffer = await readFileAsync(fullPath)

    if (noParse(path)) {
      return {
        path,
        data: buffer,
        mode,
        dir: false,
      }
    } else {
      const data = buffer.toString()
      return {
        ...await parser({ path, data, mode }, params),
        dir: false,
      }
    }
  }))
}
