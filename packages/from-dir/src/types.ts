
import { FileData } from '@initty/common'

export type FileOrDirOrBufferData = (FileData & { dir: false }) | {
  path: string
  data: Buffer
  mode: number
  dir: false
} | {
  path: string
  mode: number
  dir: true
}
